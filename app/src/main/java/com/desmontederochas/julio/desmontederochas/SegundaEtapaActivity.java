package com.desmontederochas.julio.desmontederochas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SegundaEtapaActivity extends Activity {
    private EditText etTotalMetrosPerfurados; //hf
    private EditText etDiametroExplosivo; //de
    private EditText etDensidadeExplosivo; //pe
    private EditText etCapacidadeCacamba;
    private EditText etVOD;
    private EditText etMetrosCubicosEscavados;
    private EditText etCustoMetroPerfuracao;

    private Button btExibirResultadoSegundaEtapa;

    private double volumeRochaFuro = 0.0;
    private double afastamento = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda_etapa);

        etTotalMetrosPerfurados = (EditText) findViewById(R.id.et_total_metros_perfurados);
        etDiametroExplosivo = (EditText) findViewById(R.id.et_diametro_explosivo);
        etDensidadeExplosivo = (EditText) findViewById(R.id.et_densidade_explosivo);
        etCapacidadeCacamba = (EditText) findViewById(R.id.et_capacidade_cacamba);
        etVOD = (EditText) findViewById(R.id.et_vod);
        etMetrosCubicosEscavados = (EditText) findViewById(R.id.et_metros_cubicos_escavados);
        etCustoMetroPerfuracao = (EditText) findViewById(R.id.et_custo_metro_perfuracao);

        btExibirResultadoSegundaEtapa = (Button) findViewById(R.id.bt_exibir_resultado_segunda_etapa);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String volume = extras.getString("volumerochafuro");
        volumeRochaFuro = Double.valueOf(volume);

        String afs = extras.getString("afastamento");
        afastamento = Double.valueOf(afs);

        btExibirResultadoSegundaEtapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isAllFieldsFilled()){
                    Intent intent = new Intent(SegundaEtapaActivity.this, ResultadosSegundaEtapaActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("perfuracaoespecifica", calculaPerfuracaoEspecifica() + "");
                    extras.putString("razaolinearcarregamento", calculaRazaolinearCarregamento() + "");
                    extras.putString("tampao", calculaTampao() + "");
                    extras.putString("informacaotampao", getInformacoesTampao());
                    extras.putString("cargaexplosivo", calculaCargaExplosivo() + "");
                    extras.putString("alturacargafundo", calculaAlturaCargaFundo() + "");
                    extras.putString("alturacargacoluna", calculaAlturaCargaColuna() + "");
                    extras.putString("cargatotal", calculaCargaTotal() + "");
                    extras.putString("razaocarregamento", calculaRazaoCarregamento() + "");
                    extras.putString("cargaexplosivokg", calculaCargaExplosivoKG() + "");
                    extras.putString("capacidadecacambapordiametrofuro", calculaCapacidadeCacambaPorDiametroFuro() + "");
                    extras.putString("pressaocargacolunaexplosivo", pressaoCargaColunaExplosivo() + "");
                    extras.putString("numerofurosnecessario", calculaNumeroFurosNecessario() + "");
                    extras.putString("totalmetrosperfurados", calculaTotalMetrosPerfurados() + "");
                    extras.putString("totalexplosivo", calculaTotalExplosivo() + "");
                    extras.putString("custoperfuracao", calculaCustoPerfuracao() + "");

                    intent.putExtras(extras);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "Todos os campos precisam ser preenchidos!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean isAllFieldsFilled() {
        if(etTotalMetrosPerfurados.getText().toString().trim().length() > 0 && etDiametroExplosivo.getText().toString().trim().length() > 0 &&
                etDensidadeExplosivo.getText().toString().trim().length() > 0 && etCapacidadeCacamba.getText().toString().trim().length() > 0 &&
                etVOD.getText().toString().trim().length() > 0 && etMetrosCubicosEscavados.getText().toString().trim().length() > 0 && etMetrosCubicosEscavados.getText().toString().trim().length() > 0) {
            return true;
        }
        return false;
    }

    //PE OK!
    private double calculaPerfuracaoEspecifica() {
        double pe = 0;
        double hf = Double.valueOf(etTotalMetrosPerfurados.getText().toString());
        pe = hf / volumeRochaFuro;
        return pe;
    }

    //RL OK!
    private double calculaRazaolinearCarregamento() {
        double rl = 0;
        double de = Double.valueOf(etDiametroExplosivo.getText().toString());
        double pe = Double.valueOf(etDensidadeExplosivo.getText().toString());
        rl = ((Math.PI * de * de) / 4000) * pe;
        return rl;
    }

    //Tampao T OK!
    private double calculaTampao(){
        return 0.7 * afastamento;
    }

    //OK!
    private String getInformacoesTampao(){
        if(calculaTampao() < afastamento){
            return "Probabilidade de ultralançamento";
        }else{
            return "Mais matacoes, menor probabilidade de ultralançamento";
        }
    }

    //He OK!
    private double calculaCargaExplosivo(){
        double he = 0.0;
        double hf = Double.valueOf(etTotalMetrosPerfurados.getText().toString());
        he = hf - calculaTampao();
        return he;
    }

    //Hcf OK!
    private double calculaAlturaCargaFundo(){
        double hcf = 0;
        double hf = Double.valueOf(etTotalMetrosPerfurados.getText().toString());
        hcf = 0.3 * calculaCargaExplosivo();
        return hcf;
    }

    //Hcc OK!
    private double calculaAlturaCargaColuna(){
        double hcc = 0.0;
        hcc = calculaCargaExplosivo() - calculaAlturaCargaFundo();
        return hcc;
    }

    //CT OK!
    private double calculaCargaTotal(){
        double ct = 0.0;
        ct = calculaAlturaCargaFundo() + calculaAlturaCargaColuna();
        return ct;
    }

    //Rc OK!
    private double calculaRazaoCarregamento(){
        double rc = 0.0;
        rc = calculaCargaTotal() / volumeRochaFuro;
        return rc;
    }

    //Ce OK!
    private double calculaCargaExplosivoKG(){
        double ce = 0.0;
        ce = calculaRazaolinearCarregamento() * calculaCargaExplosivo();
        return ce;
    }

    //Capacidade Caçamba m3 OK!
    private double calculaCapacidadeCacambaPorDiametroFuro(){
        double capacidade = Double.valueOf(etCapacidadeCacamba.getText().toString());
        double result = 0.032 * capacidade;
        return result;
    }

    //pf OK!
    private double pressaoCargaColunaExplosivo(){
        double pf = 0.0;
        double pe = Double.valueOf(etDensidadeExplosivo.getText().toString());
        double vod = Double.valueOf(etVOD.getText().toString());
        pf = 0.0000001 * pe * ( (vod * vod) / 4 );
        return pf;
    }

    //nf OK!
    private double calculaNumeroFurosNecessario(){
        double nf = 0.0;
        double metrosCubicosSeremEscavados = Double.valueOf(etMetrosCubicosEscavados.getText().toString());
        nf = metrosCubicosSeremEscavados / volumeRochaFuro;
        return nf;
    }

    //mp OK!
    private double calculaTotalMetrosPerfurados(){
        double mp = 0.0;
        double hf = Double.valueOf(etTotalMetrosPerfurados.getText().toString());
        mp = calculaNumeroFurosNecessario() * hf;
        return mp;
    }

    //te OK!
    private double calculaTotalExplosivo(){
        double te = 0.0;
        te = calculaNumeroFurosNecessario() * calculaCargaExplosivo();
        return te;
    }

    //cp OK!
    private double calculaCustoPerfuracao(){
        double cp = 0.0;
        double custoPerfuracao = Double.valueOf(etCustoMetroPerfuracao.getText().toString());
        cp = calculaTotalMetrosPerfurados() * custoPerfuracao;
        return cp;
    }
}
