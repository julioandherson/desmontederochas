package com.desmontederochas.julio.desmontederochas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

public class ResultadosPrimeiraEtapaActivity extends Activity {
    private TextView tvAfastamento;
    private TextView tvEspacamento;
    private TextView tvSubperfuracao;
    private TextView tvProfundidadeFuro;
    private TextView tvVolumeRochaFuro;

    private Button btAvancarSegundaEtapa;

    private String volumeRochaFuro;
    private String afastamento;

    private DecimalFormat decimalFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados_primeira_etapa);

        tvAfastamento = (TextView) findViewById(R.id.tv_afastamento);
        tvEspacamento = (TextView) findViewById(R.id.tv_espacamento);
        tvSubperfuracao = (TextView) findViewById(R.id.tv_subperfuracao);
        tvProfundidadeFuro = (TextView) findViewById(R.id.tv_profundidade_furo);
        tvVolumeRochaFuro = (TextView) findViewById(R.id.tv_volume_rocha_por_furo);

        btAvancarSegundaEtapa = (Button) findViewById(R.id.bt_avancar_segunda_etapa);

        decimalFormat = new DecimalFormat("#0.000");


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        afastamento = extras.getString("afastamento");
        String afastamentoFormatted = decimalFormat.format(Double.parseDouble(afastamento));

        String espacamento = extras.getString("espacamento");
        String espacamentoFormatted = decimalFormat.format(Double.parseDouble(espacamento));

        String subperfuracao = extras.getString("subperfuracao");
        String subperfuracaoFormatted = decimalFormat.format(Double.parseDouble(subperfuracao));

        String profundidadeFuro = extras.getString("profundidadefuro");
        String profundidadeFuroFormatted = decimalFormat.format(Double.parseDouble(profundidadeFuro));

        volumeRochaFuro = extras.getString("volumerochafuro");
        String volumeRochaFuroFormatted = decimalFormat.format(Double.parseDouble(volumeRochaFuro));


        tvAfastamento.setText(afastamentoFormatted + "m");
        tvEspacamento.setText(espacamentoFormatted + "m");
        tvSubperfuracao.setText(subperfuracaoFormatted + "m");
        tvProfundidadeFuro.setText(profundidadeFuroFormatted + "m");
        tvVolumeRochaFuro.setText(volumeRochaFuroFormatted + "m³"); //Vai para segunda etapa

        btAvancarSegundaEtapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultadosPrimeiraEtapaActivity.this, SegundaEtapaActivity.class);
                Bundle extras = new Bundle();

                extras.putString("volumerochafuro", volumeRochaFuro);
                extras.putString("afastamento", afastamento);

                intent.putExtras(extras);
                startActivity(intent);
            }
        });

    }
}
