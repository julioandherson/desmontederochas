package com.desmontederochas.julio.desmontederochas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private EditText etAlturaBancada; //Hb
    private EditText etMediaDensidadeExplosivo; //Pe
    private EditText etDensidadeRocha; //Pr
    private EditText etDiametroExplosivo; // de
    private EditText etAnguloFuro; //alpha

    private Button btExibirResultadoPrimeiraEtapa;

    private RadioGroup rgInicializacao;
    private RadioButton rbTipoDeInicializacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etAlturaBancada = (EditText) findViewById(R.id.et_altura_bancada);
        etMediaDensidadeExplosivo = (EditText) findViewById(R.id.et_media_densidade_explosivo);
        etDensidadeRocha = (EditText) findViewById(R.id.et_densidade_rocha);
        etDiametroExplosivo = (EditText) findViewById(R.id.et_diametro_explosivo);
        etAnguloFuro = (EditText) findViewById(R.id.et_angulo_furo);

        btExibirResultadoPrimeiraEtapa = (Button) findViewById(R.id.bt_exibir_resultado_primeira_etapa);

        rgInicializacao = (RadioGroup) findViewById(R.id.rg_inicializacao);

        btExibirResultadoPrimeiraEtapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllFieldsFilled()) {
                    Intent intent = new Intent(MainActivity.this, ResultadosPrimeiraEtapaActivity.class);
                    Bundle extras = new Bundle();

                    extras.putString("afastamento", calculaAfastamento() + "");
                    extras.putString("espacamento", calculaEspacamento() + "");
                    extras.putString("subperfuracao", calculaSubperfuracao() + "");
                    extras.putString("profundidadefuro", calculaProfundidadeFuro() + "");
                    extras.putString("volumerochafuro", calculaVolumeRochaPorFuro() + "");

                    intent.putExtras(extras);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Todos os campos precisam ser preenchidos!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Verifica se todos os campos estão preenchidos
     * @return true se sim, caso contrário false
     */
    private boolean isAllFieldsFilled() {
        if (etAlturaBancada.getText().toString().trim().length() > 0 && etMediaDensidadeExplosivo.getText().toString().trim().length() > 0 &&
                etDensidadeRocha.getText().toString().trim().length() > 0 && etDiametroExplosivo.getText().toString().trim().length() > 0 &&
                etAnguloFuro.getText().toString().trim().length() > 0) {
            return true;
        }
        return false;
    }

    private double calculaAfastamento() {
        double afastamento = 0;
        double pe = Double.valueOf(etMediaDensidadeExplosivo.getText().toString());
        double pr = Double.valueOf(etDensidadeRocha.getText().toString());
        double de = Double.valueOf(etDiametroExplosivo.getText().toString());
        afastamento = 0.0123 * (2 * (pe / pr) + 1.5) * de;
        return afastamento;
    }

    //TESTADO OK!
    private double calculaEspacamento() {
        double e = 0;

        int selectedId = rgInicializacao.getCheckedRadioButtonId();
        rbTipoDeInicializacao = (RadioButton) findViewById(selectedId);

        if (rbTipoDeInicializacao.getText().toString().toLowerCase().trim().equals("instantaneamente")) {
            e = 2 * calculaAfastamento(); // CASO SEJA INSTANTANEAMENTE
        } else {
            e = 1.4 * calculaAfastamento(); // CASO SEJA COM RETARDO
        }
        return e;
    }

    //TESTADO OK!
    private double calculaSubperfuracao() {
        double s = 0;
        s = 0.3 * calculaAfastamento();
        return s;
    }

    private double calculaProfundidadeFuro(){
        double hf = 0;
        double hb = Double.valueOf(etAlturaBancada.getText().toString()); //Altura da Bancada
        double angulo = Double.valueOf(etAnguloFuro.getText().toString());

        hf = (hb / Math.cos(Math.toRadians(angulo))) + ((1 - (angulo / 100)) * calculaSubperfuracao());
        return hf;
    }

    private double calculaVolumeRochaPorFuro(){
        double v = 0;
        double hb = Double.valueOf(etAlturaBancada.getText().toString()); //Altura da Bancada

        v = hb * calculaAfastamento() * calculaEspacamento();
        return v;
    }
}
