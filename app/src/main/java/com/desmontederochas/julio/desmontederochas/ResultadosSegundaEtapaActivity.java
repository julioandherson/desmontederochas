package com.desmontederochas.julio.desmontederochas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

public class ResultadosSegundaEtapaActivity extends Activity {
    private TextView tvPerfuracaoEspecifica;
    private TextView tvRazaoLinearCarregamento;
    private TextView tvTampao;
    private TextView tvInformacaoTampao;
    private TextView tvCargaExplosivo;
    private TextView tvAlturaCargaFundo;
    private TextView tvCargaColuna;
    private TextView tvCargaTotal;
    private TextView tvRazaoCarregamento;
    private TextView tvCargaExplosivoKG;
    private TextView tvCapacidadeCacambaPorDiametroFuro;
    private TextView tvPressaoCargaColunaExplosivo;
    private TextView tvNumeroFurosNecessario;
    private TextView tvTotalMetrosPerfurados;
    private TextView tvTotalExplosivo;
    private TextView tvCustoPerfuracao;

    private DecimalFormat decimalFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados_segunda_etapa);

        tvPerfuracaoEspecifica = (TextView) findViewById(R.id.tv_perfuracao_especifica);
        tvRazaoLinearCarregamento = (TextView) findViewById(R.id.tv_razao_linear_carregamento);
        tvTampao = (TextView) findViewById(R.id.tv_tampao);
        tvInformacaoTampao = (TextView) findViewById(R.id.tv_informacoes_tampao);
        tvCargaExplosivo = (TextView) findViewById(R.id.tv_carga_do_explosivo);
        tvAlturaCargaFundo = (TextView) findViewById(R.id.tv_altura_carga_fundo);
        tvCargaColuna = (TextView) findViewById(R.id.tv_carga_coluna);
        tvCargaTotal = (TextView) findViewById(R.id.tv_carga_total);
        tvRazaoCarregamento = (TextView) findViewById(R.id.tv_razao_carregamento);
        tvCargaExplosivoKG = (TextView) findViewById(R.id.tv_carga_do_explosivo_kg);
        tvCapacidadeCacambaPorDiametroFuro = (TextView) findViewById(R.id.tv_capacidade_cacamba_x_diametro_furo);
        tvPressaoCargaColunaExplosivo = (TextView) findViewById(R.id.tv_carga_coluna_explosivo);
        tvNumeroFurosNecessario = (TextView) findViewById(R.id.tv_numero_furos_necessario);
        tvTotalMetrosPerfurados = (TextView) findViewById(R.id.tv_total_metro_perfurados);
        tvTotalExplosivo = (TextView) findViewById(R.id.tv_total_explosivo);
        tvCustoPerfuracao = (TextView) findViewById(R.id.tv_custo_perfuracao);

        decimalFormat = new DecimalFormat("#0.000");

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String perfuracaoEspecifica = extras.getString("perfuracaoespecifica");
        String perfuracaoEspecificaFormatted = decimalFormat.format(Double.parseDouble(perfuracaoEspecifica));

        String razaoLinearCarregamento = extras.getString("razaolinearcarregamento");
        String razaoLinearCarregamentoFormatted = decimalFormat.format(Double.parseDouble(razaoLinearCarregamento));

        String tampao = extras.getString("tampao");
        String tampaoFormatted = decimalFormat.format(Double.parseDouble(tampao));

        String informacaoTampao = extras.getString("informacaotampao");

        String cargaExplosivo = extras.getString("cargaexplosivo");
        String cargaExplosivoFormatted = decimalFormat.format(Double.parseDouble(cargaExplosivo));

        String alturaCargaFundo = extras.getString("alturacargafundo");
        String alturaCargaFundoFormatted = decimalFormat.format(Double.parseDouble(alturaCargaFundo));

        String alturaCargaColuna = extras.getString("alturacargacoluna");
        String alturaCargaColunaFormatted = decimalFormat.format(Double.parseDouble(alturaCargaColuna));

        String cargaTotal = extras.getString("cargatotal");
        String cargaTotalFormatted = decimalFormat.format(Double.parseDouble(cargaTotal));

        String razaoCarregamento = extras.getString("razaocarregamento");
        String razaoCarregamentoFormatted = decimalFormat.format(Double.parseDouble(razaoCarregamento));

        String cargaExplosivoKG = extras.getString("cargaexplosivokg");
        String cargaExplosivoKGFormatted = decimalFormat.format(Double.parseDouble(cargaExplosivoKG));

        String capacidadeCacambaPorDiametroFuro = extras.getString("capacidadecacambapordiametrofuro");
        String capacidadeCacambaPorDiametroFuroFormatted = decimalFormat.format(Double.parseDouble(capacidadeCacambaPorDiametroFuro));

        String pressaoCargaColunaExplosivo = extras.getString("pressaocargacolunaexplosivo");
        String pressaoCargaColunaExplosivoFormatted = decimalFormat.format(Double.parseDouble(pressaoCargaColunaExplosivo));

        String numeroFurosNecessario = extras.getString("numerofurosnecessario");
        String numeroFurosNecessarioFormatted = Math.ceil(Double.parseDouble(numeroFurosNecessario)) + "";

        String totalMetrosPerfurados = extras.getString("totalmetrosperfurados");
        String totalMetrosPerfuradosFormatted = decimalFormat.format(Double.parseDouble(totalMetrosPerfurados));

        String totalExplosivo = extras.getString("totalexplosivo");
        String totalExplosivoFormatted = decimalFormat.format(Double.parseDouble(totalExplosivo));

        String custoPerfuracao = extras.getString("custoperfuracao");
        String custoPerfuracaoFormatted = decimalFormat.format(Double.parseDouble(custoPerfuracao));

        tvPerfuracaoEspecifica.setText(perfuracaoEspecificaFormatted + " m/m³");
        tvRazaoLinearCarregamento.setText(razaoLinearCarregamentoFormatted + " kg/m");
        tvTampao.setText(tampaoFormatted + " m");
        tvInformacaoTampao.setText(informacaoTampao);
        tvCargaExplosivo.setText(cargaExplosivoFormatted + " m");
        tvAlturaCargaFundo.setText(alturaCargaFundoFormatted + " m");
        tvCargaColuna.setText(alturaCargaColunaFormatted + " m");
        tvCargaTotal.setText(cargaTotalFormatted + " m");
        tvRazaoCarregamento.setText(razaoCarregamentoFormatted + " g/m³");
        tvCargaExplosivoKG.setText(cargaExplosivoKGFormatted + " kg");
        tvCapacidadeCacambaPorDiametroFuro.setText(capacidadeCacambaPorDiametroFuroFormatted + " m");
        tvPressaoCargaColunaExplosivo.setText(pressaoCargaColunaExplosivoFormatted + " GPa");
        tvNumeroFurosNecessario.setText(numeroFurosNecessarioFormatted + " furos");
        tvTotalMetrosPerfurados.setText(totalMetrosPerfuradosFormatted + " m");
        tvTotalExplosivo.setText(totalExplosivoFormatted + " m");
        tvCustoPerfuracao.setText(custoPerfuracaoFormatted + " R$/m");
    }
}
